﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MvcApplication2.Models;

namespace MvcApplication2.Controllers
{
    public class StoreController : ApiController
    {
        private StoreContext db = new StoreContext();

        // GET api/Store
        public IEnumerable<Store> GetStores()
        {
            return db.Stores.AsEnumerable();
        }

        // GET api/Store/5
        public Store GetStore(int id)
        {
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return store;
        }

        // PUT api/Store/5
        public HttpResponseMessage PutStore(int id, Store store)
        {
            if (ModelState.IsValid && id == store.StoreId)
            {
                db.Entry(store).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Store
        public HttpResponseMessage PostStore(Store store)
        {
            if (ModelState.IsValid)
            {
                db.Stores.Add(store);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, store);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = store.StoreId }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Store/5
        public HttpResponseMessage DeleteStore(int id)
        {
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Stores.Remove(null);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, store);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}